export enum FilterBy {
  Price = 'price',
  Rating = 'rating',
  TimesRated = 'timesRated',
  MinToMax = 'minToMax',
  MaxToMin = 'maxToMin',
  CurrentLocation = 'currentLocation',
  SortType = 'sortBy',
  SortByKey = 'sortByKey',
  Page = 'page'
}
