export interface Agent {
  name?: string,
  description?: string,
  location?: string,
  price?: number,
  rating?: number,
  timesRated?: number,
  images?: string
}

