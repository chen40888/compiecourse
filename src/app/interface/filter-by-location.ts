export enum FilterByLocation {
  All = 'all',
  Center = 'center',
  North = 'north',
  South = 'south'
}
