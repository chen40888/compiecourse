import {Agent} from './agent';

export interface AgentStore {
  duplicateAents?: Agent[];
  agents?: Agent[];
}

