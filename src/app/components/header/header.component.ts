import {Component, OnInit} from '@angular/core';
import {SandboxService} from '../../services/sandbox/sandbox.service';
import {AnimationType} from '../../interface/animation-type';
import {HeaderAnimation} from '../../animation/header-animation';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {FilterBy} from '../../interface/filter-by';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [HeaderAnimation]
})
export class HeaderComponent implements OnInit {
  private state: string = AnimationType.Hide;
  public isSearchOpen: boolean = true;
  private searchTerm = '';

  constructor(private agentService: SandboxService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.checkForSearchKey();
  }

 private checkForSearchKey() {
    this.route.queryParams.subscribe(
      (params) => {
        if (params[FilterBy.SortByKey]) {
          this.searchTerm = params[FilterBy.SortByKey];
        }
      });
  }

  public toggleSearch(): void {
    this.state === AnimationType.Normal ? this.state = AnimationType.Hide : this.state = AnimationType.Normal;
  }

  public searchThisString(): void {
    this.updateUrl(FilterBy.SortByKey, this.searchTerm);
  }

  public updateUrl(filterBy: FilterBy, value_to_update): void {
    const queryParams: Params = Object.assign({}, this.route.snapshot.queryParams);
    queryParams[filterBy] = value_to_update;
    queryParams[FilterBy.Page] = 1;

    this.router.navigate(['.'], {queryParams: queryParams});
  }

  public toggleModal(): void {
    this.agentService.toggleModal();
  }

}
