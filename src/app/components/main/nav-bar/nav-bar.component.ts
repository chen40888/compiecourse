import {Component, Input} from '@angular/core';
import {SandboxService} from '../../../services/sandbox/sandbox.service';
import {FilterBy} from '../../../interface/filter-by';
import {FilterByLocation} from '../../../interface/filter-by-location';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent {
  @Input() priceSort = FilterBy.MinToMax;
  @Input() countAgents: number;
  @Input() locationSort;
  public FilterByLocation = FilterByLocation;
  public FilterBy = FilterBy;

  constructor(private agentService: SandboxService, private router: Router, private route: ActivatedRoute) {
  }

  public sortBy(filert: FilterBy): void {
    (filert === FilterBy.Price) ? this.updateUrl(FilterBy.Price, this.priceSort) : this.updateUrl(FilterBy.SortType, filert);
  }

  public sortAgentsByLocation(): void {
    this.updateUrl(FilterBy.CurrentLocation, this.locationSort);
  }

  private updateUrl(filterBy: FilterBy, value_to_update): void {
    const queryParams: Params = Object.assign({}, this.route.snapshot.queryParams);
    queryParams[filterBy] = value_to_update;
    if (filterBy === FilterBy.CurrentLocation || filterBy === FilterBy.SortByKey) {
      queryParams[FilterBy.Page] = 1;
    }
    this.router.navigate(['.'], {queryParams: queryParams});
  }
}
