import {Component, OnInit} from '@angular/core';
import {Agent} from '../../interface/agent';
import {SandboxService} from '../../services/sandbox/sandbox.service';
import {Observable} from 'rxjs';
import {AnimationType} from '../../interface/animation-type';
import {MainAnimation} from '../../animation/main-anumation';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {FilterBy} from '../../interface/filter-by';
import {FilterByLocation} from '../../interface/filter-by-location';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  animations: [MainAnimation]

})
export class MainComponent implements OnInit {
  private agentsState: Observable<Agent[]>;
  private state: string = AnimationType.Hide;
  private countAgents: number;
  private locationSort = FilterByLocation.All;
  private priceSort = FilterBy.MinToMax;
  private pageCounter;
  private currentPage = 1;

  constructor(private agentService: SandboxService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.urlFilterSubscribe();
    this.agentsSubscribe();
    this.modalSubscribe();
  }

  private agentsSubscribe(): void {
    this.agentService.filteredAgents.subscribe(
      (res: any) => {
        this.agentsState = res;
        this.countAgents = res.length;
        this.pageCounter = Math.ceil(res.length / 10);
      }
    );
  }

  private urlFilterSubscribe(): void {
    this.route.queryParams.subscribe(
      (params) => {
        if (params[FilterBy.CurrentLocation]) {
          this.locationSort = params[FilterBy.CurrentLocation];
          this.agentService.sortByLocationAndSearch(params[FilterBy.CurrentLocation], null);
        }
        if (params[FilterBy.SortByKey]) {
          this.agentService.sortByLocationAndSearch(null, params[FilterBy.SortByKey]);
        }
        if (params[FilterBy.Price]) {
          this.priceSort = params[FilterBy.Price];
          this.sortByQueryParams(FilterBy.Price, params[FilterBy.Price]);
        }
        if (params[FilterBy.SortType]) {
          this.agentService.sortByType(params[FilterBy.SortType]);
        }
        if (!params[FilterBy.Page]) {
          this.updateUrl(FilterBy.Page, this.currentPage);
        } else {
          this.currentPage = params[FilterBy.Page];
        }
      });
  }

  public updateUrl(filterBy: FilterBy, value_to_update): void {
    const queryParams: Params = Object.assign({}, this.route.snapshot.queryParams);
    queryParams[filterBy] = value_to_update;
    if (filterBy === FilterBy.CurrentLocation || filterBy === FilterBy.SortByKey) {
      queryParams[FilterBy.Page] = 1;
    }
    this.router.navigate(['.'], {queryParams: queryParams});
  }

  private sortByQueryParams(sortType, price?): void {
    (sortType === FilterBy.Price ? this.agentService.sortByType(sortType, price) : this.agentService.sortByType(sortType));
  }

  public pageChange(showPage): void {
    this.currentPage = showPage;
    this.updateUrl(FilterBy.Page, showPage);
  }

  public createRange(number): number[] {
    const items: number[] = [];
    for (let i = 1; i <= number; i++) {
      items.push(i);
    }
    return items;
  }

  private modalSubscribe(): void {
    this.agentService.isModalOpenAsObserv.subscribe(
      () => {
        this.state = this.state === AnimationType.Hide ? AnimationType.Normal : AnimationType.Hide;
      });
  }
}
