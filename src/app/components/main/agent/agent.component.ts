import {Component, Input, OnInit} from '@angular/core';
import {Agent} from '../../../interface/agent';

@Component({
  selector: 'app-agent',
  templateUrl: './agent.component.html',
  styleUrls: ['./agent.component.scss']
})
export class AgentComponent {
  @Input() agent: Agent;

  constructor() { }
}
