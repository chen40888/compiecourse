import {Component, OnInit} from '@angular/core';
import {SandboxService} from '../../../services/sandbox/sandbox.service';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-wizard-modal',
  templateUrl: './wizard-modal.component.html',
  styleUrls: ['./wizard-modal.component.scss']
})
export class WizardModalComponent implements OnInit {

  select: object = {
    chooseInsurance: 'good',
    whenStartInsurance: 'good',
    selectsinsuranceSetting: 'chacha',
    selectDriver: 'good',
    carPurpose: 'good',
    choose_city: 'good',
  };

  radio: object = {
    radio1: '',
    radio2: ''
  }

  constructor(private agentService: SandboxService) {
  }

  ngOnInit() {
  }

  closeModal(): void {

    this.agentService.toggleModal();
  }

  onSubmit(value) {
    console.log(value,' form value')
  }
}
