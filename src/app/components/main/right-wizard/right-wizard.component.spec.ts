import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RightWizardComponent } from './right-wizard.component';

describe('RightWizardComponent', () => {
  let component: RightWizardComponent;
  let fixture: ComponentFixture<RightWizardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RightWizardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RightWizardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
