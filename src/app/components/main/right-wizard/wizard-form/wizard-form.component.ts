import { Component, OnInit } from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-wizard-form',
  templateUrl: './wizard-form.component.html',
  styleUrls: ['./wizard-form.component.scss']
})
export class WizardFormComponent implements OnInit {
  insuranceOptions: FormGroup;

  constructor() { }

  ngOnInit() {
    // this.setFormDefinition();
  }

  setFormDefinition() {
    this.insuranceOptions = new FormGroup({
      'radio': new FormGroup({
        'carInsurance': new FormControl(null, [Validators.required]),
        // 'email': new FormControl(null, [Validators.required, Validators.email], this.forbiddenEmails)
      }),
    });
  }
}
