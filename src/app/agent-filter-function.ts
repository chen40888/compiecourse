import {Agent} from './interface/agent';
import {SortFunctionUtils} from './utils/sort-function-utils';
import {FilterBy} from './interface/filter-by';

export function agentFilterFunction(state) {
  const agentState = state.agentsStore;

  let agentsArrayAfterSorting: Agent[] = agentState.agents;

  // console.log(state, 'state');

  if (SortFunctionUtils.isEmpty(state.agentsStore.searchKey)) {
    agentsArrayAfterSorting = SortFunctionUtils.sortByLocationAndSearch(state, null, state.agentsStore.searchKey);
  }

  agentsArrayAfterSorting = SortFunctionUtils.sortByLocationAndSearch(state, state.agentsStore.currentLocation, state.agentsStore.searchKey);

  if (agentState.filterBy.rating) {
    agentsArrayAfterSorting = SortFunctionUtils.sortByType(agentsArrayAfterSorting, FilterBy.Rating);

  }
  if (agentState.filterBy.timesRated) {
    agentsArrayAfterSorting = SortFunctionUtils.sortByType(agentsArrayAfterSorting, FilterBy.TimesRated);

  }
  agentsArrayAfterSorting = SortFunctionUtils.sortByType(agentsArrayAfterSorting, FilterBy.Price, agentState.isMinToMax);

  return agentsArrayAfterSorting;
}
