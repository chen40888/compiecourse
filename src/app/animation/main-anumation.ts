import {animate, state, style, transition, trigger} from '@angular/animations';

export const MainAnimation = [
  trigger('Modal', [
    state('normal', style({
      'opacity': '1',
      'visibility': 'visible',
      'transition': 'opacity 2s linear'

    })),
    state('hide', style({
      'opacity': '0',
      'visibility': 'hidden',
      'transition': 'opacity 2s linear'
    })),
    transition('hide <=> normal', animate(0)),
  ])
];



