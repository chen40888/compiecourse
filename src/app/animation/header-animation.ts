import {animate, state, style, transition, trigger} from '@angular/animations';

export const HeaderAnimation = [
  trigger('searchBar', [
    state('normal', style({
      'transform': 'translate(0, 30%)'
    })),
    state('hide', style({
      'transform': 'translate(0, -80px)'
    })),
    transition('normal <=> hide', animate(800)),
  ])
];



