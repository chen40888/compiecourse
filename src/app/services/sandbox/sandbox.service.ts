import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Agent} from '../../interface/agent';
import {HttpClient} from '@angular/common/http';
import {FilterBy} from '../../interface/filter-by';
import {Store} from '@ngrx/store';
import * as AgentsReducer from '../../store/main.actions';
import {ProviderService} from '../provider/provider.service';
import {agentFilterFunction} from '../../agent-filter-function';

@Injectable({
  providedIn: 'root'
})
export class SandboxService {
  public filteredAgents: Observable<Agent[]>;
  public isModalOpen = new Subject<boolean>();
  public isModalOpenAsObserv = this.isModalOpen.asObservable();

  constructor(private http: HttpClient, private store: Store<{ agentsStore: { agents: Agent[] } }>, private providerService: ProviderService) {
    this.setAgents();
    this.subscribeAgentStoreObserve();
  }

  public toggleModal(): void {
    this.isModalOpen.next();
  }

  private subscribeAgentStoreObserve(): void {
    this.filteredAgents = this.store.select<any>(agentFilterFunction);
  }

  private setAgents(): void {
    this.providerService.getAgents().subscribe(
      (res: Agent[]) => {
        this.store.dispatch(new AgentsReducer.SetAgents(res));
      });
  }

  public sortByLocationAndSearch(location, stringToSearch): void {
    if (!stringToSearch && stringToSearch !== '') {
      this.store.dispatch(new AgentsReducer.SetLocation(location));
    } else {
      this.store.dispatch(new AgentsReducer.SetSearchKey(stringToSearch));
    }
  }

  public sortByType(type: FilterBy, filerType?): void {
    if (type === FilterBy.Price) {
      const isMinToMax = (filerType === FilterBy.MinToMax);
      this.store.dispatch(new AgentsReducer.SetPriceSort(isMinToMax));
    } else {
      this.store.dispatch(new AgentsReducer.SetFilterType(type));
    }
  }
}
