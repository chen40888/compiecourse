import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Agent} from '../../interface/agent';

@Injectable({
  providedIn: 'root'
})
export class ProviderService {

  constructor(private http: HttpClient) {
  }

  send(is_get, url, params?) {
    if (!is_get) {
      return this.http.post(url, params);
    } else {
      return this.http.get(url);
    }
  }

  getAgents(): any {
    return this.http.get('./assets/agents.json');
  }
}
