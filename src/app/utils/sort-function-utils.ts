import {FilterBy} from '../interface/filter-by';
import {Agent} from '../interface/agent';
import {FilterByLocation} from '../interface/filter-by-location';
import {Locations} from '../../assets/locations';

export class SortFunctionUtils {
  private currentLocation = FilterByLocation.All;

  static isEmpty(string: string): boolean {
    return string && string.trim() !== '';
  }

  static sortByLocationAndSearch(state, location, stringToSearch): Agent[] {
    let agentsArray: Agent[],
      agentArrayAfterSort: Agent[] = [],
      currentLocation = state.agentsStore.currentLocation,
      oldStringToSearch = state.agentsStore.searchKey;
    agentsArray = state.agentsStore.agents;

    // location === null ? location = currentLocation : currentLocation = location;
    (this.isEmpty(stringToSearch) ? oldStringToSearch = stringToSearch : stringToSearch = oldStringToSearch);
    (location === FilterByLocation.All ? agentArrayAfterSort = agentsArray : agentArrayAfterSort = this.sortByLocation(agentsArray, currentLocation));

    if (stringToSearch && stringToSearch !== '') {
      agentArrayAfterSort = this.searchThis(stringToSearch, agentArrayAfterSort);
    }

    return agentArrayAfterSort;
  }

  static sortByType(sortArray: Agent[], type: FilterBy, filerType?): Agent[] {
    const agentArray: Agent[] = sortArray;

    if (agentArray.length) {
      if (type === FilterBy.Price) {
        (filerType ? agentArray.sort((a, b) => (a.price - b.price)) : agentArray.sort((a, b) => (b.price - a.price)));
      } else {
        agentArray.sort((a, b) => (a.price - b.price));
        agentArray.sort((a, b) => (b[type] - a[type]));
      }
    }
    return agentArray;
  }

  static sortByLocation(agentsArray, location) {
    let agentIndex;
    const locationsArray = Locations[location],
      agentArrayAfterSort: Agent[] = [];

    for (agentIndex = 0; agentIndex < agentsArray.length; agentIndex++) {
      if (locationsArray.find(thisLocation => thisLocation === agentsArray[agentIndex].location)) {
        agentArrayAfterSort.push(agentsArray[agentIndex]);
      }
    }
    return agentArrayAfterSort;
  }

  static searchThis(stringToSearch, arrayToSearch): Agent[] {
    const arrayAfterSort = [];

    arrayToSearch.filter((item) => {
      for (const key in item) {
        if ((typeof item[key] === 'string' || item[key] instanceof String) &&
          (item[key].indexOf(stringToSearch) !== -1)) {
          arrayAfterSort.push(item);
          break;
        }
      }
    });
    return arrayAfterSort;
  }

}
