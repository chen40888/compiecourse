import {Pipe, PipeTransform} from '@angular/core';
import {SandboxService} from '../services/sandbox/sandbox.service';

@Pipe({name: 'filter'})

export class FilterPipe implements PipeTransform {

  constructor(private agentService: SandboxService) {
  }

  count: any = 0;

  transform(value, args) {
    // console.log(value,' value from pipe')
    // console.log(args,' args from pipe')
    this.count = 0;
    if(!args[0]) {
      return value;
    } else if (value) {
      return value.filter(item => {
        for (let key in item) {
          if((typeof item[key] === 'string' || item[key] instanceof String) &&
            (item[key].indexOf(args[0]) !== -1)) {
            this.count++;
            // this.agentService.searchAgentCount.next(this.count);
            return true;
          }
        }
      });
    }
  }
}
