import {Action} from '@ngrx/store';
import {Agent} from '../interface/agent';
import {FilterBy} from '../interface/filter-by';

export const GET_AGENTS = 'GET_AGENTS';
export const SET_AGENTS = 'SET_AGENTS';
export const SET_DUPLICATE_AGENTS = 'SET_DUPLICATE_AGENTS';
export const SET_FILTER_TYPE = 'SET_DUPLICATE_AGENTS';
export const SET_PRICE_SORT = 'SET_PRICE_SORT';
export const SET_LOCATION = 'SET_LOCATION';
export const SET_SEARCH_KEY = 'SET_SEARCH_KEY';

export class GetAgents implements Action {
  constructor() {}

  readonly type = GET_AGENTS;

}

export class SetAgents implements Action {

  readonly type = SET_AGENTS;

  constructor(public payload: Agent[]) {
  }
}

export class SetDuplicateAgents implements Action {

  readonly type = SET_DUPLICATE_AGENTS;

  constructor(public payload: Agent[]) {
  }
}

export class SetFilterType implements Action {

  readonly type = SET_FILTER_TYPE;

  constructor(public payload: any) {
  }
}

export class SetPriceSort implements Action {

  readonly type = SET_PRICE_SORT;

  constructor(public payload: boolean) {
  }
}

export class SetSearchKey implements Action {

  readonly type = SET_SEARCH_KEY;

  constructor(public payload: string) {
  }
}

export class SetLocation implements Action {

  readonly type = SET_LOCATION;

  constructor(public payload: string) {
  }
}

export type AgentsReducer = GetAgents | SetAgents | SetDuplicateAgents | SetFilterType | SetPriceSort | SetLocation | SetSearchKey;
