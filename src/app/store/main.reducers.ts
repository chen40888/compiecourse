import * as AgentsReducer from './main.actions';

const initial_state = {
  agents: [],
  filterBy: {
    rating: false,
    timesRated: false
  },
  isMinToMax: false,
  searchKey: '',
  currentLocation: 'all'
};

export function agentsReducer(state = initial_state, action: AgentsReducer.AgentsReducer) {
  switch (action.type) {
    case AgentsReducer.GET_AGENTS:
      return state;
    case AgentsReducer.SET_AGENTS:
      return {
        ...state,
        agents: action.payload
      };
    case AgentsReducer.SET_LOCATION:
      return {
        ...state,
        currentLocation: action.payload
      };
    case AgentsReducer.SET_SEARCH_KEY:
      return {
        ...state,
        searchKey: action.payload
      };
    case AgentsReducer.SET_FILTER_TYPE:
      return {
        ...state,
        filterBy: {
          ...state.filterBy,
          [action.payload]: true
        }
      };
    case AgentsReducer.SET_PRICE_SORT:
      return {
        ...state,
        isMinToMax: action.payload
      };
    case AgentsReducer.SET_DUPLICATE_AGENTS:
      return {
        ...state,
        agents: action.payload
      };
    default:
      return state;
  }

}
