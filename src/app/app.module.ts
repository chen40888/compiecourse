import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {MainComponent} from './components/main/main.component';
import {NavBarComponent} from './components/main/nav-bar/nav-bar.component';
import {RightWizardComponent} from './components/main/right-wizard/right-wizard.component';
import {WizardFormComponent} from './components/main/right-wizard/wizard-form/wizard-form.component';
import {SandboxService} from './services/sandbox/sandbox.service';
import {FilterPipe} from './pipe/filter.pipe';
import {AgentComponent} from './components/main/agent/agent.component';
import {StepsHeaderComponent} from './components/main/steps-header/steps-header.component';
import {WizardModalComponent} from './components/main/wizard-modal/wizard-modal.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {StoreModule} from '@ngrx/store';
import {agentsReducer} from './store/main.reducers';
import {ProviderService} from './services/provider/provider.service';
import {NgxPaginationModule} from 'ngx-pagination';
// import { DatepickerModule as YourAlias } from 'angular-mat-datepicker'
import { DatepickerModule } from 'angular-mat-datepicker'



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MainComponent,
    NavBarComponent,
    RightWizardComponent,
    WizardFormComponent,
    FilterPipe,
    AgentComponent,
    StepsHeaderComponent,
    WizardModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgxPaginationModule,
    // DatepickerModule,
    StoreModule.forRoot({agentsStore: agentsReducer})
  ],
  providers: [SandboxService, ProviderService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
